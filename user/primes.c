/*
 * primes (moderate)/(hard)
 *
 * Write a concurrent version of prime sieve using pipes. This idea is
 * due to Doug McIlroy, inventor of Unix pipes. The picture halfway
 * down this page: http://swtch.com/~rsc/thread/ and the surrounding
 * text explain how to do it. Your solution should be in the file
 * user/primes.c.
 *
 * Your goal is to use pipe and fork to set up the pipeline. The first
 * process feeds the numbers 2 through 35 into the pipeline. For each
 * prime number, you will arrange to create one process that reads
 * from its left neighbor over a pipe and writes to its right neighbor
 * over another pipe. Since xv6 has limited number of file descriptors
 * and processes, the first process can stop at 35.
 *
 * -------------------------------------------------------------------
 *
 * Some hints:
 *
 * - Be careful to close file descriptors that a process doesn't need,
 *   because otherwise your program will run xv6 out of resources
 *   before the first process reaches 35.
 *
 * - Once the first process reaches 35, it should wait until the
 *   entire pipeline terminates, including all children,
 *   grandchildren, &c. Thus the main primes process should only exit
 *   after all the output has been printed, and after all the other
 *   primes processes have exited.
 *
 * - Hint: read returns zero when the write-side of a pipe is closed.
 *
 * - It's simplest to directly write 32-bit (4-byte) into to the
 *   pipes, rather than using formatted ASCII I/O.
 *
 * - You should create the processes in the pipeline only as they are
 *   needed.
 *
 * - Add the program to UPROGS in Makefile.
 *
*/


#include "kernel/types.h"
#include "kernel/stat.h"
#include "user/user.h"


int
main()
{
  int i, j, prime = -1, pid = -1;
  int primes[34];
  int p[2];

  /*
   * lets first build our assumed primes array: primes[0] = 2,
   * primes[1] = 3 , ...
   */
  i = 0;
  while(i < 34){
    primes[i] = i + 2;
    i++;
  }

  /*
   * the plan is to feed the assumed array to the parent process and
   * give a zero value to the multipliers of a definite prime so after
   * the first iteration we have (2,3,0,5,0,...,35) and the second
   * iteration would start from 3 and so on.
   */
  for(i = 2; i < 36 ;i++){
    for(j = i; j < 36; j++){
      // in order to start each iteration properly, it is enough to
      // find the next non-zero element in the array, ...
      if(primes[j - 1] != 0){
        // if this is the first non-zero element we have found in this
        // iteration, it is definitely a prime: create a child process
        // and create a pipe ->
        if(j == i){
          prime = j;
          primes[j - 1] = prime;
          pid = fork();
          pipe(p);
        } else if(j > i && (j % i) == 0){
        // set all multipliers of the discovered prime to zero, ->
          primes[j - 1] = 0;
        // otherwise keep the element as it is.
        } else if(j > i && (j % i) != 0){
          primes[j - 1] = j + 1;
        }
      }
    }
    /*
     * This is the pipe phase where the full array of the assumed
     * primes is written by the parent process of that iteration to
     * p[0] and then read by the child process of that iteration from
     * p[1].
     */   
    if(p[0] > 0 && p[1] > 0 && pid >=0){
      if(pid > 0){
        //printf("writing %p to p[0] for child process %d... \n", primes, pid);
        write(p[0], primes, sizeof primes);
        pid = wait((int *) 0);
        //printf("child %d is done\n", pid);
      } else if(pid == 0){
        read(p[1], primes, sizeof primes);
        printf("prime %d\n", prime);
        exit(0);
      }
      close(p[0]);
      close(p[1]);
    }
  }
 
  exit(0);

}
