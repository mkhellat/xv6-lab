/* pingpong (easy)
 *
 * Write a program that uses UNIX system calls to ''ping-pong'' a byte
 * between two processes over a pair of pipes, one for each
 * direction. The parent should send a byte to the child; the child
 * should print "<pid>: received ping", where <pid> is its process ID,
 * write the byte on the pipe to the parent, and exit; the parent
 * should read the byte from the child, print "<pid>: received pong",
 * and exit. Your solution should be in the file user/pingpong.c.
 *
 * -----------------------------------------------------------------
 *
 * Some hints:
 *
 * - Use pipe to create a pipe.
 *
 * - Use fork to create a child.
 *
 * - Use read to read from the pipe, and write to write to the pipe.
 *
 * - Use getpid to find the process ID of the calling process.
 *
 * - Add the program to UPROGS in Makefile.
 *
 * - User programs on xv6 have a limited set of library functions
 *   available to them. You can see the list in user/user.h; the
 *   source (other than for system calls) is in user/ulib.c,
 *   user/printf.c, and user/umalloc.c.
 */


#include "kernel/types.h"
#include "kernel/stat.h"
#include "user/user.h"

int
main()
{
  int pid, mypid;
  char *buf[1];
  int p[2], q[2];

  /*
   * create a paid of pipes and record read and write fds in
   * p: p[0] -> read p[1] -> write
   * q: q[0] -> read q[1] -> write
   */
  pipe(p);
  pipe(q);

  /*
   * The ONLY way to create a process in xv6 and in UNIX is fork():
   *
   * > fork() creates a 'child process' by the calling 'parent
   *   process' with exactly the same user-space memory as the 'parent
   *   process'
   * > fork() in fact gives service to two processes and consequently
   *   returns two values --
   *     - Parent Process: child's PID
   *     -  Child Process: zero
   * -----------------------------------------------------------------
   * Here, pingpong invokes fork() as a result of which a child
   * pingpong would be created which carries the same instructions,
   * data, and stack as the parent pingpong. The only difference in
   * data is in 'pid' as it is is the return value of fork() which is
   * going to be different for the parent and the child.
   */
  pid = fork();


  /*
   * UNIX and xv6 __time-shares__ processes:
   * -----------------------------------------------------------------
   * Hence, we have to check who is being served at a specific time;
   * in our case checking whether this is the parent pingpong or the
   * child pingpong.
  */
  if(pid > 0){
    /* getting the current process id */
    mypid = getpid();
    buf[0] = "1";          // the ping byte
    write(p[1], buf, 1);   // write the ping byte to p[1]
    pid = wait((int *) 0); // wait until child is done
    read(q[0], buf, 1);    // now that child is done, it mean that it
                           // has written its message (pong) to q[1]
                           // and we can read that message from q[0]
    printf("%d: received pong\n", mypid);
  } else if(pid == 0){
    /* getting the current process id */
    mypid = getpid();    
    read(p[0], buf, 1);    // read the ping byte
    printf("%d: received ping\n", mypid);
    buf[0] = "2";          // the pong byte
    write(q[1], buf, 1);   // write the pong byte to q[1]
    exit(0);               // release resources
  } else{
    printf("fork error\n");
  }
  
  exit(0);

}
