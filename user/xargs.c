/*
 * xargs (moderate)
 *
 * Write a simple version of the UNIX xargs program: read lines from
 * the standard input and run a command for each line, supplying the
 * line as arguments to the command. Your solution should be in the
 * file user/xargs.c.
 *
 * -------------------------------------------------------------------
 *
 * The following example illustrates xarg's behavior:
 *   $ echo hello too | xargs echo bye
 *   bye hello too
 * 
 * Note that the command here is "echo bye" and the additional
 * arguments are "hello too", making the command "echo bye hello too",
 * which outputs "bye hello too".
 *
 * Please note that xargs on UNIX makes an optimization where it will
 * feed more than one argument to the command at a time. We don't
 * expect you to make this optimization. To make xargs on UNIX behave
 * the way we want it to for this lab, please run it with the -n
 * option set to 1. For instance
 *
 *   $ echo "1\n2" | xargs -n 1 echo line
 *   line 1
 *   line 2
 *
 * -------------------------------------------------------------------
 *
 * Some hints:
 *
 * - Use fork and exec to invoke the command on each line of input.
 *
 * - Use wait in the parent to wait for the child to complete the
 *   command.
 *
 * - To read individual lines of input, read a character at a time
 *   until a newline ('\n') appears.
 *
 * - kernel/param.h declares MAXARG, which may be useful if you need
 *   to declare an argv array.
 *
 * - Add the program to UPROGS in Makefile.
 *
 * - Changes to the file system persist across runs of qemu; to get a
 *   clean file system run make clean and then make qemu.
 *
 */


#include "kernel/types.h"
#include "kernel/stat.h"
#include "user/user.h"
#include "kernel/fs.h"
#include "kernel/param.h"

void
xargs(char *cmd[])
{
  int pid, i, k = 0, nargs = 0;
  char **cmd_full = 0;
  char *cmd_name = "";
  char buf[512];
  char line[512];

  // we have two strings:
  // - "cmd" -> xargs arg
  // - "buf" -> read from fd 0
  while(cmd[nargs] != 0)
    nargs++;

  read(0, buf, sizeof(buf));

  // determining cmd_name and initializing cmd_full
  strcpy(cmd_name, cmd[0]);
  cmd_full = cmd;

  // determining cmd_full and cmd_args
  for(i = 0; i < strlen(buf); i++){
    if(buf[i] == '\n' || buf[i] == '\0'){
      //
      // reset/terminate line indexer to start/stop reading new line
      //
      k = 0;
      //
      // append line as arg to cmd_full
      //
      cmd_full[nargs] = line;
      //
      // execute the command
      //
      if((pid = fork()) > 0){
        pid = wait((int *) 0);        
      } else if(pid == 0){
        exec(cmd_name, cmd_full);
      } else{
        printf("fork error\n");
        exit(1);
      }
    } else{
      //
      // this is where characters of "buf" are read before EOL is
      // reached and "line" is created.
      //
      line[k] = buf[i];
      k++;
    }
  } 
}
  

int
main(int argc, char *argv[])
{
  int i;
  char **cmdarg = argv;
  
  if(argc < 2 ){
    printf("Usage: xargs [command]\n");
    exit(1);
  } else{
    for(i = 1; i < argc + 1; i++)
      cmdarg[i-1] = argv[i];
    xargs(cmdarg);
    exit(0);
  }
}
